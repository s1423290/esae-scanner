#!/usr/bin/python
# execute `python3 run.py -h` to get a more detailed explanation of how to use this program.
import pprint


# This file is a class library used by run.py, it should not be invoked seperately!

# Analysis class
class Analysis:

	# Analyse pre-processed results of Scanner
	# scan_result = list of {'error', 'valid', 'version', 'CA'}
	def analyse(scan_result):

		# init values for analysis results
		successfull_connections = 0
		valid_certificate_checks = 0
		CA_organisation_occurances = dict()
		certificate_TLS_Version = dict()


		# run through results and count different properties
		for result in scan_result:
			# input was on blocklist?
			if result['error'] == 'blocked':
				continue

			# os_error = network, host error or blocked, not TLS specific errors
			if result['error'] == 'os_error':
				continue;

			# successfull connection
			if result['error'] == False:
				successfull_connections += 1

				# valid certificate
				if result['valid'] == True:
					valid_certificate_checks += 1

					# count occurances of CA's
					if result['CA'] not in CA_organisation_occurances:
						CA_organisation_occurances[result['CA']] = 0
					CA_organisation_occurances[result['CA']] += 1

					# count of certificates TLS version
					if result['version'] not in certificate_TLS_Version:
						certificate_TLS_Version[result['version']] = 0
					certificate_TLS_Version[result['version']] += 1

		# transform collected analytical information to percentages
		for ca in CA_organisation_occurances:
			CA_organisation_occurances[ca] = (CA_organisation_occurances[ca], str(round(float(CA_organisation_occurances[ca]/valid_certificate_checks*100),2)) + '%' )

		for tlsv in certificate_TLS_Version:
			certificate_TLS_Version[tlsv] = (certificate_TLS_Version[tlsv], str(round(float(certificate_TLS_Version[tlsv]/valid_certificate_checks*100),2)) + '%' )

		# Display measured results to stdout
		print("Number of successful connections: ", successfull_connections)
		print("Number of unsuccessful connections: " + str(len(scan_result)-successfull_connections))
		print("percentage of successful connections that has a valid certificate: "+ str(valid_certificate_checks) + " = " + str(round(float(valid_certificate_checks/successfull_connections*100), 2))+"%")
		n_invalid_cert = successfull_connections-valid_certificate_checks
		print("Percentage of successful connections with invalid certificate: "+str(n_invalid_cert) + " = " + str(round(float(n_invalid_cert/successfull_connections*100), 2)) + "%")

		print("\nNumber and percentages of valid certificates that each CA issued")
		pprint.pprint(sorted(CA_organisation_occurances.items(), key=lambda d: d[1], reverse=True)) # use organizationName

		print("\nOccurances and percentages of the TLS versions used during valid connections")
		pprint.pprint(sorted(certificate_TLS_Version.items(), key=lambda d: d[1], reverse=True)) # Choose certificate TLS version over connection TLS version



# code to sort scan result based on optional property
#pprint.pprint(sorted(self.scan_result, key=lambda d: d.get('CA', False) or 'ZZZZZZZ' + str(d['error'])))