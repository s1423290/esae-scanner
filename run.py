#!/usr/bin/python
# execute `python3 run.py -h` to get a more detailed explanation of how to use this program.
import sys
import os
import getopt
import pprint
from scanner import Scanner
from analysis import Analysis


# start code, load/parse command-line arguments and initialize an instance of the Scanner class
def main(argv):
	# initialize generic scanner parameters and default optional values
	scanlist = ''
	blocklist = ''
	rootsfile = ''
	logfile = ''
	timeout = 5
	rate = 5
	verbose = False

	# parse argv
	try:
		opts, args = getopt.getopt(argv,"hi:b:r:l:t:v",["help","input=","blocklist=","roots=", "log=", "timeout=", "rate=", "verbose"])
	except getopt.GetoptError:
		display_help_instructions()
		return

	# loop through arguments to store values in proper variables
	for opt, arg in opts:
		# help - display commandline arguments
		if opt in ('-h', '--help'):
			 display_help_instructions()
			 return

		# required parameters
		elif opt in ("-i", "--input"):
			scanlist = arg
		elif opt in ("-b", "--blocklist"):
			blocklist = arg
		elif opt in ("-r", "--roots"):
			rootsfile = arg

			
		# optional parameters
		elif opt in ("-l", "--log"):
			logfile = arg
		elif opt in ("-t", "--timeout"):
			timeout = int(arg)
		elif opt in ("--rate"):
			rate = int(arg)
		elif opt in ("-v", "--verbose"):
			verbose = True


	# have all required parameters been passed?
	if not (scanlist and blocklist and rootsfile):
		display_help_instructions()
		return;

	# do all (required) files really exist?
	for file in [scanlist, blocklist, rootsfile]:
		if not os.path.isfile(file):
			print("File '"+file+"' does not exist")
			return

	# are optional arguments in proper format?
	if timeout <= 0:
		print("Timeout must be a positive number")
	if rate <= 0:
		print("Rate must be a positive number")



	# initiate scanner class and set properties!
	scanner = Scanner(scanlist, blocklist, rootsfile, timeout, rate, verbose)
	if logfile:
		scanner.setLog(logfile)

	# run scan and perform analysis over the result
	scan_result = scanner.scan()
	Analysis.analyse(scan_result)

	
	# temporary execution code for test purposes
	# scanner._scanHost('www.python.org', '151.101.36.223', 0)
	# scanner._scanHost('tls-v1-2.badssl.com','104.154.89.105', 0)
	# scanner._scanHost('adnxtr.com','3.0.25.35', 0)
	# scanner._scanHost('nationalgeographic.com','35.83.33.73', 0)
	# scanner._scanHost('playtem.com','178.33.61.40', 0)
	# scanner._scanHost('somd.org','35.208.172.227', 0)



# This function displays the command-line usage of this file.
def display_help_instructions():
	print(sys.argv[0] + " -i <scanlist> -b <blocklist> -r <roots-store> [-l <logfile> -t <timeout> --rate=<rate> -v -h]\n")
	print("=========== Mandatory arguments ===========")
	print("\t -i <scanlist>, --input=<scanlist>")
	print("\t\t <scanlist> is a file with domain,ip on every line, all entries will be scanned\n")

	print("\t -b <blocklist>, --blocklist=<blocklist>")
	print("\t\t <blocklist> is a file with domains or ip-addresses on every line that should not be scanned by the program\n")

	print("\t -r <roots-store>, --roots=<roots-store>")
	print("\t\t <roots-store> is the roots store containing CA root certifices, used to verify certificate chains\n")

	print("=========== Optional arguments ===========")
	print("\t -l <logfile>, --log=<logfile>")
	print("\t\t <logfile> is the file used to write log data. If this file already exists, it will be truncated and re-used\n")
	print("\t -t <timeout>, --timeout=<timeout>")
	print("\t\t <timeout> Number of seconds before a connection is closed. Default value is 5 seconds\n")
	print("\t --rate=<rate>")
	print("\t\t <rate> Maximum number of connections made per second. Default value is 5\n")
	print("\t -v, --verbose")
	print("\t\t Displays detailed information to stdout during scanning phase\n")

	print("\t -h, --help")
	print("\t\t Display the this help message and stop program execution.\n")









# all initialisation and function definitions have been done, call main function to start execution.
if __name__ == "__main__":
	 main(sys.argv[1:]);