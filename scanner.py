#!/usr/bin/python
# execute `python3 run.py -h` to get a more detailed explanation of how to use this program.
import ssl
import socket
import pprint
import ipaddress
import os
import time
import csv
import json

# This file is a class library used by run.py, it should not be invoked seperately!


# Scanner class
class Scanner:

	# CSV headers used by logfile
	logfile_csv_headers = ['domain', 'ip', 'error', 'valid', 'version', 'CA', 'raw']

	# initialize all properties of Scanner class
	def __init__(self, scanlist, blocklist, rootsfile, timeout, rate, verbose):
		# store important variables
		self.scanlist	=	scanlist
		self.blocklist = blocklist
		self.rootsfile = rootsfile
		self.timeout = timeout
		self.rate = rate
		self.verbose = verbose
		self.log = None

		# create proper SSL context - verification settings ensure that all certificate properties will be confirmed (signature, dates and root certificate)
		# ssl.PROTOCOL_TLS_CLIENT = OP_ALL|OP_NO_SSLv3|OP_NO_SSLv2|OP_CIPHER_SERVER_PREFERENCE|OP_SINGLE_DH_USE|OP_SINGLE_ECDH_USE|OP_NO_COMPRESSION
		# thereby supporting all protocols except SSLv2 and SSLv3, so all TLSv1.0-v1.3 are supported
		self.context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)

		# reference the root store so that certificate chains can be validated on the fly (CERT)
		self.context.load_verify_locations(self.rootsfile) # source of root certificates

		# If set supported TLS versions does not work
		# check https://www.programcreek.com/python/example/106813/ssl.html

		# process blocklist
		self.blocked_domains = set() # blocked domains
		self.blocked_ips = set() # IPv4Address
		self.blocklistModifiedTime = 0 # used for NONBLOCK: reloading blocklist during scan
		self._parseBlocklist()

		# load scan list and create result list
		self.input = list() # format: [(domain, ip), ..., ]
		self.scan_result = list() # pre-processed scan results for each entry in input
		self._loadScanList()

	# set log file, open csv writer and write headers
	def setLog(self, logfile):
		# open csv log and write headers
		self.log = csv.writer(open(logfile, 'w', newline=''))
		self.log.writerow(Scanner.logfile_csv_headers)



	# contains the real program loop. Scans all input and collects their analysis information.
	# returns the scan result, which has been pre-processed for analysis component in _scanHost
	# allow at most self.rate connections per second
	def scan(self):
		# create new result list
		self.scan_result = [None] * len(self.input)
		time_cmp = time.time()
		scans_since_last_time = 0

		for i in range(0, len(self.input)):
			# BONBLOCK: can reload block list while scanning, without restart.
			if self.blocklistModifiedTime != os.path.getmtime(self.blocklist):
				if self.verbose:
					print("UPDATE BLOCKLIST")

				self._parseBlocklist()


			# RATE: allow max of self.rate connections per second, if exceeded wait with while-loop until time has passed
			time_new = time.time()
			if scans_since_last_time >= self.rate and time_new-time_cmp < 1:
				while time_new-time_cmp < 1:
					time.sleep(0.1)
					time_new = time.time()

			if time_new-time_cmp >= 1:
				time_cmp = time_new
				scans_since_last_time = 0

			scans_since_last_time += 1

			# scan host
			self._scanHost(self.input[i][0], self.input[i][1], i) # input[i] = (domain,ip) 

		return self.scan_result
		

	# scan a single host, collect their certificate information, pre-process and store it for the analysis component
	def _scanHost(self, hostname, ip, result_index):

		if self.verbose:
			print("==== HOST " + str(result_index) + ": " + hostname)

		# Test whether this host or ip is on the blocklist
		if self._isBlocked(hostname, ip):
			if self.verbose:
				print("BLOCKED: ", ip)
			self.scan_result[result_index] = {
				'error' : 'blocked'
			}
			# write to logfile
			# logfile_csv_headers = ['domain', 'ip', 'error', 'valid', 'version', 'CA', 'raw']
			if self.log:
				self.log.writerow([
					hostname,
					ip,
					'blocked',
					'',
					'',
					'',
					''
				])
			return


		# create a socket to connect with host and obtain the hosts certificate information (TLS)
		conn = self.context.wrap_socket(socket.socket(socket.AF_INET), server_hostname=hostname)
		conn.settimeout(self.timeout) 

		# init variable in method scope
		cert = None

		# start the connection, check for potential errors
		try:
			conn.connect((ip, 443)) 

			# retrieve TLS certificate information and TLS connection information
			cert = conn.getpeercert()
			tls_version = conn.version()

			# print certificate information in verbose mode
			if self.verbose:
				pprint.pprint(cert)	

			
			# close socket
			conn.close()

			# write data to result list
			self.scan_result[result_index] = {
				'error': False, # os_error = network, host error or blocked, not TLS specific errors
				'valid' : True, # success hear means that ssl.SSLCertVerificationError has not been thrown, thus the (hostname and chain) verification has succeeded
				'version' : tls_version,
				'CA' : ''
			}
			# Add CA(issuer) organisation name to scan result
			for t in cert['issuer']:
				if t[0][0] == 'organizationName':
					self.scan_result[result_index]['CA'] = t[0][1]

		# Create scan results for the different exceptions
		except ssl.SSLCertVerificationError:
			# SSLCertVerificationError means that the certificate verification failed (either hostname, chain verification or other details)
			self.scan_result[result_index] = {
				'error' : False,
				'valid' : False, 
			}
			if self.verbose:
				print("SSL certificate is invalid: " + hostname)
		except socket.timeout:
			self.scan_result[result_index] = {
				'error' : 'os_error',
			}
			if self.verbose:
				print("Connection timeout")
		except ConnectionRefusedError:
			self.scan_result[result_index] = {
				'error' : 'os_error',
			}
			if self.verbose:
				print("Connection refused")
		except OSError:
			self.scan_result[result_index] = {
				'error' : 'os_error',
			}
			if self.verbose:
				print("OS Error")

		# write to logfile
		# logfile_csv_headers = ['domain', 'ip', 'error', 'valid', 'version', 'CA', 'raw']
		if self.log:
			self.log.writerow([
				hostname,
				ip,
				self.scan_result[result_index].get('error'),
				self.scan_result[result_index].get('valid'),
				self.scan_result[result_index].get('version'),
				self.scan_result[result_index].get('CA'),
				json.dumps(cert)
			])


	# load inputs from scanlist file and store in self.input
	def _loadScanList(self):
		res = list()

		for line in open(self.scanlist):
			parts = line.split(',')
			# valid format (domain,ip)
			if len(parts) == 2:
				(domain,ip) = parts
				res.append((domain.strip(), ip.strip()))

			# wrong entry in input file
			else:
				if len(line.strip()) > 0:
					print("scanlist file content: '"+line+'" has invalid format')

		self.input = res
		self.scan_result = [None] * len(self.input)



	# This method will read the blocklist and write its contents to class variables self.blocked_domains/self.blocked_ips
	def _parseBlocklist(self):
		# temporary variables
		domains = set()
		ips = set()

		# loop through blocklist and store ip's and domain names 
		for line in open(self.blocklist):
			# attempt to create ipv4 address
			try:
				ips.add(ipaddress.IPv4Network(line.strip()))
			# error means `line` is domain name
			except ipaddress.AddressValueError:
				domains.add(line.strip())

		# write temporary variables to self
		self.blocked_domains = domains
		self.blocked_ips = ips

		# latest file update time, used to notice blocklist updates
		self.blocklistModifiedTime = os.path.getmtime(self.blocklist)
		

	# see if domain/ip address is in self.blocked_domains/self.blocked_ips
	def _isBlocked(self, domain, ip):
		ip = ipaddress.IPv4Network(ip)

		# see if ip address is in blocked_ips
		for ip_entry in self.blocked_ips:
			if ip.subnet_of(ip_entry):
				return True

		# see if domain is in blocked_domains
		for domain_entry in self.blocked_domains:
			if domain.endswith(domain_entry):
				return True

		return False
