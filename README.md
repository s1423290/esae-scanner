# ESAE-scanner

TLS/X.509 scanner and analyzertest

# Execution
Run `python3 run.py -h` to see all flags/options that can be used by the scanner.
```
run.py -i <scanlist> -b <blocklist> -r <roots-store> [-l <logfile> -t <timeout> --rate=<rate> -v -h]

=========== Mandatory arguments ===========
	 -i <scanlist>, --input=<scanlist>
		 <scanlist> is a file with domain,ip on every line, all entries will be scanned

	 -b <blocklist>, --blocklist=<blocklist>
		 <blocklist> is a file with domains or ip-addresses on every line that should not be scanned by the program

	 -r <roots-store>, --roots=<roots-store>
		 <roots-store> is the roots store containing CA root certifices, used to verify certificate chains

=========== Optional arguments ===========
	 -l <logfile>, --log=<logfile>
		 <logfile> is the file used to write log data. If this file already exists, it will be truncated and re-used

	 -t <timeout>, --timeout=<timeout>
		 <timeout> Number of seconds before a connection is closed. Default value is 5 seconds

	 --rate=<rate>
		 <rate> Maximum number of connections made per second. Default value is 5

	 -v, --verbose
		 Displays detailed information to stdout during scanning phase

	 -h, --help
		 Display the this help message and stop program execution.
		 

```